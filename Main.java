import java.util.Arrays;

public class Main {

    public static int[] maximizeArray(int[] A, int[] B, int n) {
        int[] R = new int[n];
        Arrays.sort(A);
        Arrays.sort(B);
        int index_a = n - 1, index_b = n - 1, index = 0;
        while (index < n) {
            if (A[index_a] >= B[index_b]) {
                R[index] = A[index_a];
                index_a--;
            } else {
                R[index] = B[index_b];
                index_b--;
            }
            index++;
        }
        return R;
    }
    public static void main(String[] args) {
        int A[] = {2, 4, 3};
        int B[] = {5, 6, 1};
        int[] R = maximizeArray(A, B, A.length);
        for (int e : R)
            System.out.print(e + " ");

    }
}
